package com.ardab.quarkus.producer.controller;

import com.ardab.quarkus.producer.model.Account;
import com.ardab.quarkus.producer.service.ProducerService;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/api")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ProducerController {

    @Inject
    ProducerService producerService;

    @POST
    public Response produceMessage(Account account){
        producerService.sendMessage(account);
        return Response.accepted(account).build();
    }
}
