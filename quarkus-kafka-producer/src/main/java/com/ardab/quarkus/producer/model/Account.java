package com.ardab.quarkus.producer.model;

import java.util.Currency;

public class Account {

    private String iban;
    private String name;
    private String surname;
    private double balance;
    private Currency currency;

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Account(String iban, String name, String surname, double balance, Currency currency) {
        this.iban = iban;
        this.name = name;
        this.surname = surname;
        this.balance = balance;
        this.currency = currency;
    }

    public Account() {
    }

    @Override
    public String toString() {
        return "Account{" +
                "iban='" + iban + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", balance=" + balance +
                ", currency=" + currency +
                '}';
    }
}
