package com.ardab.quarkus.producer.service;

import com.ardab.quarkus.producer.model.Account;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.JSONPObject;
import io.smallrye.reactive.messaging.kafka.Record;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class ProducerService {

    public static final Logger logger = LoggerFactory.getLogger(ProducerService.class);
    @Inject
    @Channel("accounts-out")
    Emitter<Account> emitter;

    public void sendMessage(Account account){
        emitter.send(account);
        logger.info("Message sent to kafka: {}",account.toString());
    }
}
