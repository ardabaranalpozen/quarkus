package com.ardab.quarkus.consumer.service;

import com.ardab.quarkus.consumer.model.Account;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class ConsumerService {

    public static final Logger logger = LoggerFactory.getLogger(ConsumerService.class);

    @Incoming("accounts-in")
    public void receiveMessage(ConsumerRecord<?,Account> record){
        logger.info("Recieved message {}",record.value());
        logger.info("Partition : {}",record.partition());
        logger.info("Topic : {}",record.topic());
    }
}
