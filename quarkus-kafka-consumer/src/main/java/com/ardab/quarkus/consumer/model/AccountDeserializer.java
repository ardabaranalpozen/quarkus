package com.ardab.quarkus.consumer.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.quarkus.kafka.client.serialization.ObjectMapperDeserializer;

public class AccountDeserializer extends ObjectMapperDeserializer<Account> {
    public AccountDeserializer(){
        super(Account.class);
    }
}
